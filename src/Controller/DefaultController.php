<?php
declare(strict_types=1);

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        dd([
            'hostname' => gethostname(),
            'version' => getenv('APP_VERSION'),
            'env' => getenv()
        ]);
    }

    /**
     * @Route("/healthcheck")
     * @return Response
     */
    public function healthAction(): Response
    {
        return new Response('OK!');
    }
}