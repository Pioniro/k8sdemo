FROM php:7.3-fpm as base

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get -y --no-install-recommends install \
    git \
    zip \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

RUN curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/bin

COPY . /project

WORKDIR /project

FROM base as prod

RUN composer install -o --no-progress --no-scripts --no-dev --prefer-dist